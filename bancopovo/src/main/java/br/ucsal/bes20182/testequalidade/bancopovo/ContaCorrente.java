package br.ucsal.bes20182.testequalidade.bancopovo;

import java.util.List;

import org.jbehave.core.junit.JUnitStories;

public class ContaCorrente extends JUnitStories{

	private Double saldo;

	public ContaCorrente() {
		saldo = 0d;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	public void sacar(Double valor) throws SaldoInsuficienteException {
		if (valor > saldo) {
			throw new SaldoInsuficienteException();
		}
		saldo -= valor;
	}

	public Double consultarSaldo() {
		return saldo;
	}

	@Override
	protected List<String> storyPaths() {
		// TODO Auto-generated method stub
		return null;
	}

}
