Narrativa:
Como um correntista
desejo realizar um dep�sito
de modo que possa aumentar meu saldo

Cen�rio: Realizar um dep�sito numa conta nova
Dado que abri uma conta corrente
Quando realizo um dep�sito de 300 reais
Ent�o o saldo da minha conta ser� de 300 reais

Cen�rio: Realizar dois dep�sitos numa conta nova
Dado que abri uma conta corrente
Quando realizo um dep�sito de 300 reais
E realizo um dep�sito de 200 reais
Ent�o o saldo da minha conta ser� de 500 reais