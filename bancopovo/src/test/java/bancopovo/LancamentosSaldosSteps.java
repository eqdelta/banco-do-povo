package bancopovo;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class LancamentosSaldosSteps {
	@Given("um aluno est� matriculado na disciplina")
	public void instanciarAluno() {
		// Comportamento de teste
	}

	@When("informo a nota $nota")
	public void informarNota(Double nota) {
		// Comportamento de teste
	}

	@Then("a situa��o do aluno � $situacao")
	public void verificarSituacaoAluno(String situacao) {
		// Verifica��o com Assert
	}
}
